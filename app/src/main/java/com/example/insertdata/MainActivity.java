package com.example.insertdata;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;


public class MainActivity extends AppCompatActivity {

    TextView employee_list;
    Button btnadd, btnedit, btndelete;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    CollectionReference dbref = db.collection("Userdata");

    private EmployeesAdapter eAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        employee_list = findViewById(R.id.employee_list);
        btnadd = findViewById(R.id.button);

        GenerateRecyclerView();
        
        //panggil addactivity untuk add employee
        btnadd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intToAdd = new Intent(MainActivity.this, AddActivity.class);
                startActivity(intToAdd);
            }
        });
    }

    private void GenerateRecyclerView() {
        Query data = dbref.orderBy("Username");
        FirestoreRecyclerOptions<Employees>options = new FirestoreRecyclerOptions.Builder<Employees>().setQuery(data, Employees.class).build();

        eAdapter = new EmployeesAdapter(options);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(eAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        eAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        eAdapter.stopListening();
    }


}
